#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, request, abort
from modules.main_handler import MainHandler, response_create

import json

app = Flask(__name__)
main_handler = MainHandler()


@app.route("/upstream/<endpoint>", methods=["GET", "POST"])
def status(endpoint):
    if request.method == "GET":
        if endpoint == "status":
            return main_handler.get_us_status(request.args)
        else:
            abort(400, "Your endpoint is not valid.")
    elif request.method == "POST":
        try:
            if endpoint == "control":
                operation = request.json["operation"]
                if operation == "add":
                    return main_handler.server_add_to_us(
                        request.json["upstream"],
                        request.json["host"],
                        request.json["port"],
                        request.json["extras"]
                    )
                elif operation == "delete":
                    return main_handler.server_delete_from_us(
                        request.json["upstream"],
                        request.json["host"],
                        request.json["port"]
                    )
                elif operation == "modify":
                    _type = request.json["type"]
                    if _type == "server":
                        return main_handler.modify_to_us(
                            request.json["upstream"],
                            _type,
                            request.json["changes"],
                            request.json["host"],
                            request.json["port"]
                        )
                    elif _type == "upstream":
                        return main_handler.modify_to_us(
                            request.json["upstream"],
                            _type,
                            request.json["changes"]
                        )
        except KeyError as e:
            abort(400, "Missing argument.Argument is: " + str(e))


@app.errorhandler(404)
def page_not_found(e):
    return response_create(json.dumps({"ERROR": str(e)})), 404


@app.errorhandler(400)
def bad_request(e):
    return response_create(json.dumps({"ERROR": str(e)})), 400


@app.errorhandler(401)
def bad_request(e):
    return response_create(json.dumps({"ERROR": str(e)})), 401


@app.errorhandler(405)
def unauthorized_login(e):
    return response_create(json.dumps({"ERROR": str(e)})), 405


@app.errorhandler(500)
def server_error(e):
    return response_create(json.dumps({"ERROR": str(e)})), 500


if __name__ == '__main__':
    app.run(port=5000, debug=True)
