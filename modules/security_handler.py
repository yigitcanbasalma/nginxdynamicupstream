#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import abort
from IPy import IP


def is_valid_ip(_ip):
    try:
        return bool(IP("{0}/32".format(_ip)))
    except ValueError:
        abort(400, "Your IP address is invalid.")


def is_valid_port(_port):
    if _port not in xrange(65535):
        abort(400, "Your port is out of usable range.")
    return True


def us_server_extras_is_valid(extras):
    valid_values = {
        "weight": (int, range(1, 1000), "Weight attribute is not valid."),
        "max_fails": (int, range(1, 5), "Max fails attribute is not valid."),
        "fail_timeout": (int, range(1, 5), "Fail timeout attribute is not valid."),
        "max_conns": (int, range(1, 1000), "Max conns attribute is not valid."),
        "backup": (bool, (True, False), "Backup attribute is not valid."),
        "up": (bool, (True, False), "Up attribute is not valid."),
        "down": (bool, (True, False), "Down attribute is not valid."),
        "loadbalance_method": ((str, unicode), ("least_conn", "ip_hash", "round-robin"), "Load balance method attribute is not valid."),
        "sticky_method": (list, ("hash", "sticky"), "Session persistence method attribute is not valid.")
    }
    if not isinstance(extras, dict):
        abort(400, "Extra attribute must be a dict object.")
    for k, v in extras.iteritems():
        validate = valid_values[k]
        if not isinstance(v, validate[0]) or v not in validate[1]:
            if isinstance(v, list) and v[0] in validate[1]:
                continue
            abort(400, validate[2])
    return True
