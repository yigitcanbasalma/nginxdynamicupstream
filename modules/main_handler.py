#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Response, abort
from nginx import loadf
from security_handler import is_valid_ip, us_server_extras_is_valid, is_valid_port
from subprocess import Popen, PIPE

import json
import os


def response_create(data, rtype="json"):
    avail_rsp = {
        "json": "application/json"
    }
    return Response(data, mimetype=avail_rsp[rtype])


def run_local_script(script):
    lp = Popen([script], stderr=PIPE, stdout=PIPE, shell=True)
    output = lp.communicate()
    return lp.returncode, output


class MainHandler(object):
    def __init__(self):
        self.cfg = None
        self.load_config(os.getcwd() + "/modules/config.json")

    def load_config(self, file_path):
        with open(file_path, "r") as config:
            self.cfg = json.load(config)

    def get_configuration_list(self, zone):
        try:
            return loadf(self.cfg["upstream_define"][zone]).as_dict
        except KeyError:
            abort(400, "Your upstream name was not found.")

    def save_config_file(self, zone, config):
        conf_file_state = list()
        run_local_script("cp -a {0} /tmp".format(self.cfg["upstream_define"][zone]))
        for i in range(len(config["conf"])):
            for u in config["conf"][i].keys():
                cfg = u + " {\n"
                for d in config["conf"][i][u]:
                    cfg += "\t{0} {1};\n".format(d.keys()[0], d.values()[0])
                cfg += "}\n"
                conf_file_state.append(cfg)
        with open(self.cfg["upstream_define"][zone], "w") as c_file:
            c_file.write("\n".join(conf_file_state))
        r_code, out = run_local_script("nginx -t")
        if r_code == 0:
            run_local_script("kill -HUP $(cat /var/run/nginx.pid)")
            run_local_script("rm -rf /tmp/{0}".format(self.cfg["upstream_define"][zone].split("/")[-1]))
            return True
        run_local_script("mv /tmp/{0} {1}".format(self.cfg["upstream_define"][zone].split("/")[-1], self.cfg["upstream_define"][zone]))
        abort(500, "Config file error.Error: {0}".format(out[1]))

    @staticmethod
    def generate_us_info(config, us_name, verbose=False):
        p = list()
        if verbose:
            for i in range(len(config["conf"])):
                for u in config["conf"][i].keys():
                    if u.split()[1] == us_name:
                        us_data = dict()
                        us_data["UpstreamName"] = u.split()[1]
                        us_data["Servers"] = list()
                        for d in config["conf"][i][u]:
                            if d.keys()[0] != "#":
                                if d.keys()[0] == "server":
                                    server_key, server_data, server_data_dict = d.values()[0].split()[0], d.values()[0].split()[1:], dict()
                                    server_data_dict[server_key] = dict()
                                    for e in server_data:
                                        try:
                                            k, v = e.split("=")
                                        except ValueError:
                                            k, v = e, True
                                        finally:
                                            server_data_dict[server_key][k.capitalize()] = v
                                    us_data["Servers"].append(server_data_dict)
                                elif d.keys()[0] in ["least_conn", "ip_hash", "round-robin"]:
                                    us_data["LoadBalancingMethod"] = "{0}".format(d.keys()[0])
                                elif d.keys()[0] == "zone":
                                    name, size = d.values()[0].split()
                                    us_data["SharedZone"] = {
                                        "Name": name,
                                        "Size": size
                                    }
                                elif d.keys()[0] in ["hash", "sticky"]:
                                    us_data["StickyMethod"] = "{0} {1}".format(d.keys()[0], d.values()[0])
                                else:
                                    us_data[d.keys()[0].capitalize()] = d.values()[0]
                        p.append(us_data)
        else:
            for i in range(len(config["conf"])):
                for u in config["conf"][i].keys():
                    if u.split()[1] == us_name:
                        us_data = dict()
                        us_data["Servers"] = list()
                        for d in config["conf"][i][u]:
                            if d.keys()[0] != "#":
                                if d.keys()[0] == "server":
                                    us_data["Servers"].append(d.values()[0].split()[0])
                                elif d.keys()[0] in ["hash", "least_conn", "ip_hash", "sticky"]:
                                    us_data["LoadBalancingMethod"] = "{0} {1}".format(d.keys()[0], d.values()[0])
                        p.append(us_data)
        return p

    def get_us_status(self, args):
        us_config = self.get_configuration_list(args.get("upstream"))
        p = {
            "UpstreamConfig": self.generate_us_info(
                us_config, args.get("upstream"), bool(args.get("verbose"))
            )
        }
        return response_create(json.dumps(p))

    def server_add_to_us(self, us_name, host, port, extras=None):
        is_valid_ip(host)
        is_valid_port(port)
        us_config = self.get_configuration_list(us_name)
        new_server = {
            "server": ["{0}:{1}".format(host, port)]
        }
        if extras is not None:
            us_server_extras_is_valid(extras)
            for k, v in extras.iteritems():
                if isinstance(v, bool):
                    if k == "up" and "down" in new_server["server"] or k == "down" and "up" in new_server["server"]:
                        abort(400, "Up and Down states could not use same time.")
                    if k != "up":
                        new_server["server"].append(k)
                else:
                    new_server["server"].append("{0}={1}".format(k, v))
        for i in range(len(us_config["conf"])):
            for u in us_config["conf"][i].keys():
                if u.split()[1] == us_name:
                    for s in us_config["conf"][i][u]:
                        new = "{0}:{1}".format(host, port)
                        if s.keys()[0] == "server" and new == s.values()[0].split()[0]:
                            abort(400, "This server and port already exists.")
                    us_config["conf"][i][u].insert(-1, {"server": " ".join(new_server["server"])})
        self.save_config_file(us_name, us_config)
        p = {
            "Status": "OK",
            "Message": "Server added to upstream."
        }
        return response_create(json.dumps(p))

    def server_delete_from_us(self, us_name, host, port):
        is_valid_ip(host)
        is_valid_port(port)
        us_config = self.get_configuration_list(us_name)
        for i in range(len(us_config["conf"])):
            for u in us_config["conf"][i].keys():
                if u.split()[1] == us_name:
                    for s in us_config["conf"][i][u]:
                        new = "{0}:{1}".format(host, port)
                        if s.keys()[0] == "server" and new == s.values()[0].split()[0]:
                            us_config["conf"][i][u].remove(s)
        self.save_config_file(us_name, us_config)
        p = {
            "Status": "OK",
            "Message": "Server deleted to upstream."
        }
        return response_create(json.dumps(p))

    def modify_to_us(self, us_name, _type, changes, host=None, port=None):
        us_config = self.get_configuration_list(us_name)
        if _type == "upstream":
            us_server_extras_is_valid(changes)
            for i in range(len(us_config["conf"])):
                for u in us_config["conf"][i].keys():
                    if u.split()[1] == us_name:
                        if "loadbalance_method" in changes:
                            for d in us_config["conf"][i][u]:
                                if d.keys()[0] in ["least_conn", "ip_hash", "round-robin"]:
                                    us_config["conf"][i][u].remove(d)
                                    break
                            us_config["conf"][i][u].insert(1, {changes["loadbalance_method"]: ""})
                        if "sticky_method" in changes:
                            method, _val = changes["sticky_method"]
                            for d in us_config["conf"][i][u]:
                                if d.keys()[0] in ["hash", "sticky"]:
                                    us_config["conf"][i][u].remove(d)
                                    break
                            us_config["conf"][i][u].insert(len(us_config["conf"][i][u]), {method: _val})
        elif _type == "server":
            is_valid_ip(host)
            is_valid_port(port)
            us_server_extras_is_valid(changes)
            new_server = {
                "server": ["{0}:{1}".format(host, port)]
            }
            for k, v in changes.iteritems():
                if isinstance(v, bool):
                    if k == "up" and "down" in new_server["server"] or k == "down" and "up" in new_server["server"]:
                        abort(400, "Up and Down states could not use same time.")
                    if k != "up" and v:
                        new_server["server"].append(k)
                else:
                    new_server["server"].append("{0}={1}".format(k, v))
            for i in range(len(us_config["conf"])):
                for u in us_config["conf"][i].keys():
                    if u.split()[1] == us_name:
                        for d in us_config["conf"][i][u]:
                            if d.keys()[0] == "server":
                                server = "{0}:{1}".format(host, port)
                                server_prop = d.values()[0].split()
                                index = us_config["conf"][i][u].index(d)
                                if server == server_prop[0]:
                                    for pr in server_prop[1:]:
                                        if pr not in new_server["server"]:
                                            if pr == "down" and "up" not in changes:
                                                new_server["server"].append(pr)
                                            elif pr == "backup" and "backup" not in changes:
                                                new_server["server"].append(pr)
                                            elif pr not in ["backup", "down"]:
                                                new_server["server"].append(pr)
                                    us_config["conf"][i][u].remove(d)
                                    us_config["conf"][i][u].insert(index, {"server": " ".join(new_server["server"])})
                                    break
        self.save_config_file(us_name, us_config)
        p = {
            "Status": "OK",
            "Message": "{0} properties modified.".format(_type.capitalize())
        }
        return response_create(json.dumps(p))
