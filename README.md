# NginxDynamicUpstream

## Bağımlılıklar
* Python
	* python-nginx
	* flask
	* IPy

## Konfigürasyon Dosyası
* upstream_define
	* <upstream_name>: </path/to/upstream/conf>

```json
{
  "upstream_define": {
    "qa_so_web_servers": "/etc/nginx/conf.d/Config/Qa/qa_selleroffice_upstream.conf",
    "qa_so_was_servers": "/etc/nginx/conf.d/Config/Qa/qa_selleroffice_upstream.conf"
  }
}
```

## Amaç
Nginx sunucularında, reload işlemiyle çözülebilecek konfigürasyon değişikliklerini, api yardımıyla yamanıza olanak sağlamaktır.

## Çalışma Aşamaları
1. Mevcut konfigürasyon dosyasının yedeğini al.
2. Değişiklikleri yap.
3. Konfigürasyon dosyasını kontrol et.(nginx -t)
4. Sorun yoksa reload işlemini yap.Sorun varsa yedeklenen konfigürasyon dosyasını yerine taşı.

## Çalıştırma
 :red_circle: Uygulama root (UID==0) kullanıcısıyla çalışmalıdır.Aksi halde nginx işlemine kill sinyali gönderemeyeceği için sürekli hata dönecektir.
```bash
python main.py
```

## Nginx Konfigürasyonu
Bu alanda anlatılan konfigürasyon, virtual host oluşturularak, kontrol uygulaması için bir endpoint oluşturmaktır.
### nginx.conf
```bash
http {
    ...

    # Dynamic upstream
    include     /etc/nginx/conf.d/default/dynamic_upstream.conf;
    
    ...
}
```

### dynamic_upstream.conf
```bash

server {
    access_log off;
    allow 127.0.0.1;
    allow 10.10.10.0/24; # Client IP
    deny all;

    location / {
       proxy_pass   http://127.0.0.1:5000/;
    }
}
```

## API Referans
### GET /upstream/status
* Parametreler
    * upstream: Konfigürasyon dosyasında yer alan upstream ismi.
    * verbose: Ayrıntılı bilgi.
```bash
[ghost@localhost modules]$ curl "http://127.0.0.1:5000/upstream/status?upstream=qa_so_web_servers" 2> /dev/null | python -m json.tool
{
    "UpstreamConfig": [
        {
            "LoadBalancingMethod": "hash $remote_addr",
            "Servers": [
                "172.18.182.13:80",
                "172.18.182.12:80",
                "172.18.182.11:80",
                "172.18.184.41:80"
            ]
        },
        {
            "Servers": [
                "172.18.181.12:80",
                "172.18.181.11:80",
                "172.18.182.13:80"
            ]
        }
    ]
}
```

```bash
[ghost@localhost modules]$ curl "http://127.0.0.1:5000/upstream/status?upstream=qa_so_web_servers&verbose=true" 2> /dev/null | python -m json.tool
{
    "UpstreamConfig": [
        {
            "Keepalive": "32",
            "LoadBalancingMethod": "round-robin",
            "Servers": [
                {
                    "172.18.182.13:80": {
                        "Backup": true,
                        "Down": true,
                        "Weight": "2"
                    }
                },
                {
                    "172.18.182.12:80": {
                        "Backup": true,
                        "Down": true,
                        "Weight": "2"
                    }
                },
                {
                    "172.18.182.11:80": {
                        "Backup": true,
                        "Down": true,
                        "Weight": "2"
                    }
                },
                {
                    "172.18.184.41:80": {}
                }
            ],
            "SharedZone": {
                "Name": "qa_so_upstream",
                "Size": "64k"
            },
            "StickyMethod": "hash $remote_addr",
            "UpstreamName": "qa_so_web_servers"
        },
        {
            "Keepalive": "32",
            "Servers": [
                {
                    "172.18.181.12:80": {
                        "Backup": true,
                        "Down": true,
                        "Weight": "2"
                    }
                },
                {
                    "172.18.181.11:80": {
                        "Backup": true,
                        "Down": true,
                        "Weight": "2"
                    }
                },
                {
                    "172.18.182.13:80": {
                        "Backup": true,
                        "Max_conns": "200",
                        "Weight": "4"
                    }
                }
            ],
            "SharedZone": {
                "Name": "qa_so_was_upstream",
                "Size": "64k"
            },
            "UpstreamName": "qa_so_was_servers"
        }
    ]
}
```

### POST /upstream/control
* Parametreler
    * Mandatory
		* upstream_name -> <str> Konfigürasyon dosyasında yer alan upstream ismi.
		* operation -> <str> Yapılmak istenen işlem ismi.Kullanılabilir işlemler: ["add", "delete", "modify"]
		* definition -> <dict> Yapılacak işlemlerin belirtildiği alan.
			* operation == "add"
				* host -> <str> Server IP yada server dns adı.
				* port -> <int> Hizmet port numarası.
				* extras -> <dict> Ek parametreler. (Opsiyonel)
					* weight -> <int>
					* failt_timeout -> <int>
					* max_fails -> <int>
					* max_conns -> <int>
					* backup -> <bool>
					* up or down -> <bool>
			* operation == "delete"
				* host -> <str> Server IP yada server dns adı.
				* port -> <int> Hizmet port numarası.
			* operation == "modify"
				* type -> <str> Değiştirilecek objenin tipi.Kullanılabilir opsiyonlar ["server", "upstream"]
					* type == "server"
						* host -> <str> Server IP yada server dns adı.
						* port -> <int> Hizmet port numarası.
						* changes -> <dict> Değiştirilecek objeler ve yeni değerleri.
							* weight -> <int>
							* failt_timeout -> <int>
							* max_fails -> <int>
							* max_conns -> <int>
							* backup -> <bool>
							* up or down -> <bool>
					* type == "upstream"
						* changes -> <dict> Değiştirilecek objeler ve yeni değerleri.
							* loadbalance_method -> <str> Load balance methodu
							* sticky_method -> <list> First: Method name, Second: Method value

#### Add server to upstream
##### Request Body
```json
{
	"upstream": "qa_so_was_servers",
	"operation": "add",
	"host": "172.18.182.16",
	"port": 80,
	"extras": {
		"weight": 2,
		"backup": true,
		"down": true
	}
}
```
##### Response Body
```json
{
    "Message": "Server added to upstream.",
    "Status": "OK"
}
```

#### Delete server to upstream
##### Request Body
```json
{
	"upstream": "qa_so_was_servers",
	"operation": "delete",
	"host": "172.18.181.41",
	"port": 80
}
```
##### Response Body
```json
{
    "Message": "Server deleted to upstream.",
    "Status": "OK"
}
```

#### Modify server to upstream
##### Request Body
```json
{
	"upstream": "qa_so_was_servers",
	"operation": "modify",
	"type": "server",
	"host": "172.18.182.13",
	"port": 80,
	"changes": {
		"up": true,
		"weight": 4,
		"backup": true,
		"max_conns": 200
	}
}
```
##### Response Body
```json
{
    "Message": "Server properties modified.",
    "Status": "OK"
}
```
